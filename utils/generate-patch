#!/usr/bin/env bash

set -e

# Based on:
# - https://gist.github.com/serdroid/7bd7e171681aa17109e3f350abe97817
# - https://about.gitlab.com/blog/2017/11/02/automating-boring-git-operations-gitlab-ci/
# - https://stackoverflow.com/a/55344804

if [[ -z $BRANCH ]]; then
    echo '$BRANCH must be set via environment variables'
    exit 1
fi

if [ -z "$(git status --porcelain | grep "M RELEASED")" ]; then
    echo "Working directory clean"
    exit 1
else
    echo "Uncommitted changes, generating patch"

    # Check if branch already exists in remote
    if [[ -z "$(git ls-remote --heads origin $BRANCH)" ]]; then
        echo "Creating branch $BRANCH"
        git checkout -b $BRANCH
    else
        echo "Branch $BRANCH already exists in remote, reusing it by adding another commit"
        git stash
        git checkout .
        git checkout -b $BRANCH
        git pull origin $BRANCH
        git reset --hard origin/$BRANCH
        git stash pop
    fi

    echo "Commiting changes"
    git commit -m '[CI SKIP] Update RELEASED file (auto generated commit)' RELEASED

    if [[ -z $CI_GIT_TOKEN ]]; then
        echo ''
        echo '$CI_GIT_TOKEN must be defined via build variables'
        echo 'Go to https://gitlab.com/profile/personal_access_tokens and create an API token with'
        echo ' api, write_repository scopes. Then add it under Variables in the following page:'
        echo ' https://gitlab.com/jyrno42/cert-manager-patcher/-/settings/ci_cd'
        echo ''
        exit 1
    fi

    echo "Pushing changes to remote"
    git push "https://${GITLAB_USER_LOGIN}:${CI_GIT_TOKEN}@${CI_REPOSITORY_URL#*@}" $BRANCH
fi
