REPOSITORY_URL = 'https://github.com/jetstack/cert-manager.git'

CYAN ?= \033[0;36m
COFF ?= \033[0m

.PHONY: all help clean clone reset build-tags

all: help

help:
	@echo -e "+--------------------------------------Configuration--------------------------------------+"
	@echo -e "+ REPOSITORY_URL: $(REPOSITORY_URL)"
	@echo -e "+-----------------------------------------------------------------------------------------+"

clean:
	rm -rf workspace/cert-manager

clone: workspace/cert-manager reset

reset: workspace/cert-manager
	cd workspace/cert-manager && git reset --hard
	cd workspace/cert-manager && git checkout master
	cd workspace/cert-manager && git fetch origin
	cd workspace/cert-manager && git reset --hard origin/master
	rm -rf workspace/cert-manager/do-build.sh

build-tags: clone
	@echo -e "${CYAN}Building new tags${COFF}"
	cd workspace/cert-manager && \
	git --no-pager tag -l --color=never | sort -V -r | ../../strip.py | ../../bob

workspace/cert-manager:
	git clone -b master --single-branch ${REPOSITORY_URL} workspace/cert-manager
